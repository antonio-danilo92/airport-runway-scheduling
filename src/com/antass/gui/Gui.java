package com.antass.gui;
import static org.junit.Assert.assertNotNull;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Desktop;

import javax.swing.JPanel;
import javax.swing.JLabel;

import java.awt.FlowLayout;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.antass.core.EuristicaAirportScheduling;
import com.antass.core.Greedy;
import com.antass.core.Soluzione;
import com.antass.core.TwoOpt;
import com.antass.core.Greedy.flags;
import com.antass.exception.InammissibilitaException;

import java.awt.GridLayout;

import javax.swing.JProgressBar;

public class Gui {

	private JFrame frame;
	private String path;
	private double peso1;
	private double peso2;
	private ArrayList<Soluzione> soluzioni;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui window = new Gui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		final JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File("istanze"));
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 450);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panelNorth = new JPanel();
		frame.getContentPane().add(panelNorth, BorderLayout.NORTH);
		panelNorth.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panelNorth.add(panel, BorderLayout.NORTH);
		
		JLabel labelTitolo = new JLabel("Airport runway scheduling");
		labelTitolo.setFont(new Font("Tahoma", Font.ITALIC, 18));
		labelTitolo.setHorizontalAlignment(SwingConstants.CENTER);
		panelNorth.add(labelTitolo, BorderLayout.NORTH);
		
		JPanel panelFile = new JPanel();
		panelNorth.add(panelFile, BorderLayout.CENTER);
		panelFile.setLayout(new FlowLayout(FlowLayout.CENTER, 30, 5));
		
		JLabel labelNomeFile = new JLabel("Nome File:");
		labelNomeFile.setHorizontalAlignment(SwingConstants.LEFT);
		panelFile.add(labelNomeFile);
		
		final JTextArea textFile = new JTextArea("		");
		textFile.setRows(1);
		panelFile.add(textFile);
		
		final JTree tree = new JTree();
		tree.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode() {
				{
					DefaultMutableTreeNode node_1;
					node_1 = new DefaultMutableTreeNode("Greedy");
					add(node_1);
					node_1 = new DefaultMutableTreeNode("Ammissibile");
					add(node_1);
					node_1 = new DefaultMutableTreeNode("Ottimizzata");
					add(node_1);
					
				}
			}
		));
		JPanel panelTree= new JPanel();
		panelTree.add(tree);
		frame.getContentPane().add(panelTree, BorderLayout.WEST);
		
		JButton buttonSfoglia = new JButton("Sfoglia");
		panelFile.add(buttonSfoglia);
		buttonSfoglia.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				int returnValue= fc.showOpenDialog(frame);
				if(returnValue==JFileChooser.APPROVE_OPTION) {
		            path=(fc.getSelectedFile().getParent()+ "\\"+ fc.getSelectedFile().getName());
		            textFile.setText(fc.getSelectedFile().getName()+"\t");       
				}
			}	
		});
		
		JPanel panelSouth = new JPanel();
		frame.getContentPane().add(panelSouth, BorderLayout.SOUTH);
		panelSouth.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		final JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		progressBar.setVisible(false);
		
		final JLabel MessageLabel = new JLabel("New label");
		MessageLabel.setVisible(false);
		panelSouth.add(MessageLabel);
		panelSouth.add(progressBar);
		
		JPanel panelRisultati = new JPanel();
		frame.getContentPane().add(panelRisultati, BorderLayout.CENTER);
		panelRisultati.setLayout(new GridLayout(3,1));
		
		JPanel panelRiga1 = new JPanel();
		panelRisultati.add(panelRiga1);
		panelRiga1.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		JLabel labelFunzOb = new JLabel("Valore funzione obiettivo");
		labelFunzOb.setHorizontalAlignment(SwingConstants.CENTER);
		panelRiga1.add(labelFunzOb);
		final JLabel labelValoreFunzOb = new JLabel("");
		panelRiga1.add(labelValoreFunzOb);
		
		JPanel panelRiga2 = new JPanel();
		panelRisultati.add(panelRiga2);
		panelRiga2.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		JLabel labelAmmissibile = new JLabel("Ammissibile? ");
		panelRiga2.add(labelAmmissibile);
		final JLabel labelValoreAmm = new JLabel("");
		panelRiga2.add(labelValoreAmm);
		
		JPanel panelRiga3 = new JPanel();
		panelRisultati.add(panelRiga3);
		panelRiga3.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		JLabel labelSforamento = new JLabel("Eventuale sforamento ");
		panelRiga3.add(labelSforamento);
		final JLabel labelValoreSf = new JLabel("");
		panelRiga3.add(labelValoreSf);
		
		JButton buttonClean = new JButton("Clean");
		panelSouth.add(buttonClean);
		buttonClean.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent arg0) {
				textFile.setText("		");
				soluzioni= null;
				labelValoreFunzOb.setText("");
				labelValoreSf.setText(" ");
				labelValoreAmm.setText("");
				tree.setModel(new DefaultTreeModel(
						new DefaultMutableTreeNode() {
							{
								DefaultMutableTreeNode node_1;
								node_1 = new DefaultMutableTreeNode("Greedy");
								add(node_1);
								node_1 = new DefaultMutableTreeNode("Ammissibile");
								add(node_1);
								node_1 = new DefaultMutableTreeNode("Ottimizzata");
								add(node_1);
								
							}
						}
					));
			}	
		});
		
		JButton buttonRun = new JButton("Run");
		panelSouth.add(buttonRun);
		
		
		
		
		buttonRun.addActionListener(new ActionListener(){
			// non ci vorrebbe tipo una classe AlgoritmoTotale che fa tutto questo?
			public void eseguiAlgoritmo() {
				progressBar.setVisible(true);
				new Thread(){
					
					@Override
					public void run() {
							try {
								soluzioni= EuristicaAirportScheduling.calcola(path);
							} catch (NullPointerException  e) {
								System.out.println(e.getMessage());
								JOptionPane.showMessageDialog(frame, "Errore apertura file, controlla che il file selezionato sia corretto", "Error", JOptionPane.ERROR_MESSAGE);
							} catch (InammissibilitaException e1) {
								JOptionPane.showMessageDialog(frame, "L'algoritmo non � riuscito a trovare ", "Error", JOptionPane.ERROR_MESSAGE);
							} catch (InvalidFormatException e1) {
								JOptionPane.showMessageDialog(frame, "Errore apertura file, controlla che il file selezionato non � del formato richiesto", "Error", JOptionPane.ERROR_MESSAGE);
							} catch (IOException e1) {
								System.out.println(e1.getMessage());
								JOptionPane.showMessageDialog(frame, "Errore apertura file, controlla che il file selezionato sia corretto", "Error", JOptionPane.ERROR_MESSAGE);
							}
						progressBar.setVisible(false);
					}

				
				}.start();
				
			}
			public void actionPerformed(ActionEvent arg0) {
					this.eseguiAlgoritmo();
					
					 tree.setModel(new DefaultTreeModel(
			        			new DefaultMutableTreeNode(textFile.getText()) {
			        				{
			        					
			        					DefaultMutableTreeNode node_1;
			        					node_1 = new DefaultMutableTreeNode("Greedy");
			        					add(node_1);
			        					node_1 = new DefaultMutableTreeNode("Ammissibile");
			        					add(node_1);
			        					node_1 = new DefaultMutableTreeNode("Ottimizzata");
			        					add(node_1);
			        					node_1 = new DefaultMutableTreeNode("Results");
			    						node_1.add(new DefaultMutableTreeNode("Results"+textFile.getText()));
			    						add(node_1);
			        				}
			        			}
			        		));
					
					tree.addTreeSelectionListener(new TreeSelectionListener() {
					    public void valueChanged(TreeSelectionEvent e) {
					        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
					                           tree.getLastSelectedPathComponent();

					    /* if nothing is selected */ 
					        if (node == null) return;

					    /* retrieve the node that was selected */ 
					        String nodeInfo =(String) node.getUserObject();
					    /* React to the node selection. */
					        if(nodeInfo.equalsIgnoreCase("Greedy")){
					        	if(soluzioni!=null){
					        		Soluzione greedy= soluzioni.get(0);
					        		visualizzaRisultati(greedy);
					        	}	
					        }
					        if(nodeInfo.equalsIgnoreCase("Ammissibile")){
					        	if(soluzioni!=null){
					        		Soluzione greedy= soluzioni.get(1);
					        		visualizzaRisultati(greedy);
					        	}
					        }
					        if(nodeInfo.equalsIgnoreCase("Ottimizzata")){
					        	if(soluzioni!=null){
					        		Soluzione greedy= soluzioni.get(2);
					        		visualizzaRisultati(greedy);
					        	}
					        }
					        if(nodeInfo.equalsIgnoreCase("Results"+textFile.getText())){
					        	File f=new File("results/Risultato.xlsx");
					        	Desktop dt = Desktop.getDesktop();
					        	if(progressBar.isVisible()){
					        		MessageLabel.setVisible(true);
					        		MessageLabel.setText("Algoritmo ancora in esecuzione. Il file verr� aperto al termine");
					 
					        	}
					            while(progressBar.isVisible()){
					            	try {
										Thread.sleep(1000);
									} catch (InterruptedException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
					            };
				        		MessageLabel.setVisible(false);

										try {
											dt.open(f);
										} catch (IOException e1) {
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}

					            	
					        }
					    }

					        private void visualizzaRisultati(Soluzione s) {
								labelValoreFunzOb.setText(s.getValoreFunzioneObiettivo()+"");
								labelValoreSf.setText(s.getSforamento()+" ");
								labelValoreAmm.setText(s.isAmmissibile()+"");
							}
					});
				
			
				 
			}	
		});
		
		
	}
	

}
