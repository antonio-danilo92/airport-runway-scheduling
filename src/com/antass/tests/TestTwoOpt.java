package com.antass.tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Before;
import org.junit.Test;

import com.antass.core.Greedy;
import com.antass.core.Greedy.flags;
import com.antass.core.Soluzione;
import com.antass.core.TwoOpt;
import com.antass.exception.InammissibilitaException;

public class TestTwoOpt {
	private double peso1;
	private double peso2;
	@Before
	public void setUp() throws Exception {
		peso1= 1;
		peso2= 0;
		assertTrue(peso1+peso2==1);
		
	}
	@Test
	public void testTwoOptFlagEarliestFile1() throws IOException, InammissibilitaException {
		String nomeFileLettura="istanze/Arlanda-Run1.xlsx";
		String noneFileScittura="results/TestTwoOptFile1.xlsx";
		Greedy g;
		try {
			g = new Greedy(nomeFileLettura, peso1, peso2, flags.FLAG_EARLIEST);
			assertNotNull(g);
			Soluzione s= g.eseguiAlgoritmo();
			s.stampaSoluzione(noneFileScittura, "Greedy");
			TwoOpt to=new TwoOpt(nomeFileLettura);
			Soluzione ammissibile=to.ammissibile(s);
			ammissibile.stampaSoluzione(noneFileScittura, "Ammissibilita");
			Soluzione ottimizzata=to.ottimizza(ammissibile);
			ottimizzata.stampaSoluzione(noneFileScittura, "Ottimizzazione");
			
			assertTrue(ammissibile.isAmmissibile());
			assertTrue(ottimizzata.isAmmissibile());
			assertTrue(ottimizzata.getValoreFunzioneObiettivo()<=ammissibile.getValoreFunzioneObiettivo());
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Test
	public void testTwoOptFlagEarliestFile3() throws IOException, InammissibilitaException {
		String nomeFileLettura="istanze/Arlanda-Run3.xlsx";
		String noneFileScittura="results/TestTwoOptFile3.xlsx";
		Greedy g;
		try {
			g = new Greedy(nomeFileLettura, peso1, peso2, flags.FLAG_EARLIEST);
			assertNotNull(g);
			Soluzione s= g.eseguiAlgoritmo();
			s.stampaSoluzione(noneFileScittura, "Greedy");
			TwoOpt to=new TwoOpt(nomeFileLettura);
			Soluzione ammissibile=to.ammissibile(s);
			ammissibile.stampaSoluzione(noneFileScittura, "Ammissibilita");
			int valAmmiss=ammissibile.getValoreFunzioneObiettivo();

			Soluzione ottimizzata=to.ottimizza(ammissibile);
			ottimizzata.stampaSoluzione(noneFileScittura, "Ottimizzazione");
			int valOtt=ottimizzata.getValoreFunzioneObiettivo();
			assertTrue(ammissibile.isAmmissibile());
			assertTrue(ottimizzata.isAmmissibile());
			assertTrue(valOtt<=valAmmiss);
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testTwoOptFlagEarliestFile2() throws IOException, InammissibilitaException {
		String nomeFileLettura="istanze/Arlanda-Run2.xlsx";
		String noneFileScittura="results/TestTwoOptFile2.xlsx";
		Greedy g;
		try {
			g = new Greedy(nomeFileLettura, peso1, peso2, flags.FLAG_EARLIEST);
			assertNotNull(g);
			Soluzione s= g.eseguiAlgoritmo();
			s.stampaSoluzione(noneFileScittura, "Greedy");
			TwoOpt to=new TwoOpt(nomeFileLettura);
			Soluzione ammissibile=to.ammissibile(s);
			ammissibile.stampaSoluzione(noneFileScittura, "Ammissibilita");
			Soluzione ottimizzata=to.ottimizza(ammissibile);
			ottimizzata.stampaSoluzione(noneFileScittura, "Ottimizzazione");
			
			assertTrue(ammissibile.isAmmissibile());
			assertTrue(ottimizzata.isAmmissibile());
			assertTrue(ottimizzata.getValoreFunzioneObiettivo()<ammissibile.getValoreFunzioneObiettivo());
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
