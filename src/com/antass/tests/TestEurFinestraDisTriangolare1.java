package com.antass.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Before;
import org.junit.Test;

import com.antass.core.Aereo;
import com.antass.core.EuristicaChiusuraFinestre;
import com.antass.core.Greedy;
import com.antass.core.TwoOpt;
import com.antass.core.Greedy.flags;
import com.antass.core.LetturaDati;
import com.antass.core.Soluzione;
import com.antass.exception.InammissibilitaException;
import com.google.common.collect.Table;

public class TestEurFinestraDisTriangolare1 {
	private double peso1;
	private double peso2;
	@Before
	public void setUp() throws Exception {
		peso1= 0.8;
		peso2= 0.2;
		assertTrue(peso1+peso2==1);
		
	}
	
	@Test
	public void testTwoOptEarlistSwapFile1() throws IOException, InammissibilitaException {
		String nomeFileLettura="istanze/Arlanda-Run1.xlsx";
		String noneFileScittura="results/Test1FinestraDisuguaglianza.xlsx";
		Greedy g;
		try {
			g = new Greedy(nomeFileLettura, peso1, peso2, flags.FLAG_EARLIEST,flags.FLAG_SWAP);
		
		assertNotNull(g);
		Soluzione s= g.eseguiAlgoritmo();
		s.stampaSoluzione(noneFileScittura, "Risultato Greedy ES");
		TwoOpt to=new TwoOpt(nomeFileLettura);
		Soluzione ammissibile=to.ammissibile(s);
		ammissibile.stampaSoluzione(noneFileScittura, "Risultato AmmissibilitÓ ES");
		Soluzione ottimizzata=to.ottimizza(ammissibile);
		ottimizzata.stampaSoluzione(noneFileScittura, "Risultato ottimizzazione ES");
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//EuristicaChiusuraFinestre ecf= new EuristicaChiusuraFinestre("istanze/Arlanda-Run3.xlsx");
		//s= ecf.ottimizza(ottimizzata);
		//assertNotNull(s);
		//s.stampaSoluzione(noneFileScittura, "Risultato Finestra Disuguaglianza ES");
		/*s= ecf.ottimizza(ottimizzata);
		assertNotNull(s);
		s.stampaSoluzione(noneFileScittura, "Risultato Finestra ES");
		assertTrue(ammissibile.isAmmissibile());
		assertTrue(ottimizzata.isAmmissibile());
		assertTrue(ottimizzata.getValoreFunzioneObiettivo()<ammissibile.getValoreFunzioneObiettivo());
	*/
	}

	
}
