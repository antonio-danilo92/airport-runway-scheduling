package com.antass.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Before;
import org.junit.Test;

import com.antass.core.Aereo;
import com.antass.core.Greedy;
import com.antass.core.Greedy.flags;
import com.antass.core.LetturaDati;
import com.antass.core.Soluzione;
import com.google.common.collect.Table;

public class TestGreedy {
	private double peso1;
	private double peso2;
	@Before
	public void setUp() throws Exception {
		peso1= 0.9;
		peso2= 0.1;
		assertTrue(peso1+peso2==1);
		
	}
	@Test
	public void testFile1() {
		Greedy g;
		try {
			g = new Greedy("istanze/Arlanda-Run1.xlsx", peso1, peso2);
			assertNotNull(g);
			Soluzione s= g.eseguiAlgoritmo();
			assertNotNull(s);
			try {
				s.stampaSoluzione("results/Run1ResultsNoFlags"+peso1+peso2+".xlsx");
			} catch (IOException e) {
				fail("Stampa FileNotFoundException");
			}
		} catch (InvalidFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	@Test
	public void testFile2() {
		Greedy g;
		try {
			g = new Greedy("istanze/Arlanda-Run2.xlsx", peso1, peso2);
			assertNotNull(g);
			Soluzione s= g.eseguiAlgoritmo();
			assertNotNull(s);
			try {
				s.stampaSoluzione("results/Run2ResultsNoFlags"+peso1+peso2+".xlsx");
			} catch (IOException e) {
				fail("Stampa FileNotFoundException");
			}
		} catch (InvalidFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	@Test
	public void testFile3() {
		Greedy g;
		try {
			g = new Greedy("istanze/Arlanda-Run3.xlsx", peso1, peso2);
			assertNotNull(g);
			Soluzione s= g.eseguiAlgoritmo();
			assertNotNull(s);
			try {
				s.stampaSoluzione("results/Run3ResultsNoFlags"+peso1+peso2+".xlsx");
			} catch (IOException e) {
				fail("Stampa FileNotFoundException");
			}
		} catch (InvalidFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

}
