package com.antass.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Before;
import org.junit.Test;

import com.antass.core.Aereo;
import com.antass.core.LetturaDati;
import com.antass.core.Soluzione;
import com.antass.util.Hour;
import com.google.common.collect.Table;

public class TestSoluzione {
	LetturaDati ld;
	Soluzione  soluzione;
	@Before
	public void setUp() throws Exception {
		File f=new File("istanze/Arlanda-Run2.xlsx");
		soluzione= new Soluzione(f);
		assertTrue(soluzione.getSize()==0);

		System.out.println(f.getAbsolutePath());
		assertNotNull(f);
		assertTrue(f.exists());
		ld=new LetturaDati(f);
		assertNotNull(ld);
	}

	@Test
	public void testSoluzione() {
		ArrayList<Aereo> aerei;
		try {
			aerei = ld.leggiAerei();
			assertNotNull(aerei);
			for(Aereo a : aerei){
				soluzione.addAereo(a, Hour.parse("12.00.25"));
			}
			System.out.println(soluzione);
			assertTrue(soluzione.getSize()==40);
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testSoluzioneClone() {
		ArrayList<Aereo> aerei;
		try {
			aerei = ld.leggiAerei();
			assertNotNull(aerei);
			for(Aereo a : aerei){
				soluzione.addAereo(a, Hour.parse("12.00.25"));
			}
			System.out.println(soluzione);
			assertTrue(soluzione.getSize()==40);
			Soluzione clone=soluzione.clone();
			for(int i=0;i<soluzione.getSize();i++){
				boolean equals=soluzione.getAereo(i).equals(clone.getAereo(i));
				assertTrue(equals);
			}
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
