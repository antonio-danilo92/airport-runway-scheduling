package com.antass.tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Before;
import org.junit.Test;

import com.antass.core.Greedy;
import com.antass.core.Greedy.flags;
import com.antass.core.Soluzione;
import com.antass.core.TwoOpt;
import com.antass.exception.InammissibilitaException;

public class TestMigliorPesoGreedy {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testMigliorFunzObiettivo() {
		double peso1 = 0.0;
		double peso2 = 1.0;
		int nFile = 1;
		String fileName = "istanze/Arlanda-Run%d.xlsx";
		DebugSol[] dss = new DebugSol[3];
		while (nFile <= 3) {
			while (peso1 <= 1.0) {
				Greedy g;
				try {
					g = new Greedy(String.format(fileName, nFile), peso1,peso2,flags.FLAG_EARLIEST,flags.FLAG_SWAP);
					assertNotNull(g);
					Soluzione s = g.eseguiAlgoritmo();
					assertNotNull(s);
					DebugSol currSol=new DebugSol(s, peso1, peso2);
					//System.out.println(currSol);
					if (dss[nFile - 1] == null || dss[nFile - 1].s.getValoreFunzioneObiettivo() >= s.getValoreFunzioneObiettivo()) {
						dss[nFile - 1] = new DebugSol(s, peso1, peso2);
					}
					peso1 += 0.1;
					peso2 -= 0.1;
				} catch (InvalidFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			nFile++;
			peso1 = 0.0;
			peso2 = 1.0;
		}
		assertNotNull(dss[0]);
		assertNotNull(dss[1]);
		assertNotNull(dss[2]);
		System.out.println("Migliore per file 1 "+dss[0]);
		System.out.println("Migliore per file 2 "+dss[1]);
		System.out.println("Migliore per file 3 "+dss[2]);

	}
	@Test
	public void testMinorSforamento() {
		double peso1 = 0.0;
		double peso2 = 1.0;
		int nFile = 1;
		String fileName = "istanze/Arlanda-Run%d.xlsx";
		DebugSol[] dss = new DebugSol[3];
		while (nFile <= 3) {
			while (peso1 <= 1.0) {
				Greedy g;
				try {
					g = new Greedy(String.format(fileName, nFile), peso1,peso2,flags.FLAG_EARLIEST,flags.FLAG_SWAP);
					assertNotNull(g);
					Soluzione s = g.eseguiAlgoritmo();
					assertNotNull(s);
					DebugSol currSol=new DebugSol(s, peso1, peso2);
					//System.out.println(currSol);
					if (dss[nFile - 1] == null || dss[nFile - 1].s.getSforamento() >= s.getSforamento()) {
						dss[nFile - 1] = new DebugSol(s, peso1, peso2);
					}
					peso1 += 0.1;
					peso2 -= 0.1;
				} catch (InvalidFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			nFile++;
			peso1 = 0.0;
			peso2 = 1.0;
		}
		assertNotNull(dss[
		                  0]);
		assertNotNull(dss[1]);
		assertNotNull(dss[2]);
		System.out.println("Minor sforamento totale "+dss[0]);
		System.out.println("Minor sforamento totale "+dss[1]);
		System.out.println("Minor soframento totale "+dss[2]);

	}
	
	@Test
	public void testMinorFunzioneObiettivoConTwoOpt() {
		double peso1 = 0.0;
		double peso2 = 1.0;
		int nFile = 1;
		String fileName = "istanze/Arlanda-Run%d.xlsx";
		DebugSol[] dss = new DebugSol[3];
		while (nFile <= 3) {
			while (peso1 <= 1.0) {
				Greedy g;
				try {
					g = new Greedy(String.format(fileName, nFile), peso1,peso2);
					TwoOpt to=new TwoOpt(String.format(fileName, nFile));
					assertNotNull(g);
					Soluzione s = g.eseguiAlgoritmo();
					try {
						s=to.ammissibile(s);
						//s=to.ottimizza(s);
						if (dss[nFile - 1] == null || dss[nFile - 1].s.getValoreFunzioneObiettivo() > s.getValoreFunzioneObiettivo()) {
							dss[nFile - 1] = new DebugSol(s, peso1, peso2);
						}
					} catch (InammissibilitaException e) {
						System.err.println("Impossibile rendere ammissibile la soluzione del problema "+nFile+" con pesi"+ peso1+ " "+peso2);
					}
					assertNotNull(s);
					//DebugSol currSol=new DebugSol(s, peso1, peso2);
					//System.out.println(currSol);
					
					peso1 += 0.1;
					peso2 -= 0.1;
				} catch (InvalidFormatException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
			nFile++;
			peso1 = 0.0;
			peso2 = 1.0;
		}
		assertNotNull(dss[0]);
		assertNotNull(dss[1]);
		assertNotNull(dss[2]);
		System.out.println("Minor funzione obiettivo totale "+dss[0]);
		System.out.println("Minor funzione obiettivo totale "+dss[1]);
		System.out.println("Minor funzione obiettivo totale "+dss[2]);

	}
	private class DebugSol{
		public DebugSol(Soluzione s, double peso1, double peso2) {
			this.s = s;
			this.peso1 = peso1;
			this.peso2 = peso2;
		}
		@Override
		public String toString() {
			return "DebugSol [Valore funzione obiettivo=" + s.getValoreFunzioneObiettivo() + " ,Valore sforamento: "+ s.getSforamento()+", peso1=" + peso1 + ", peso2=" + peso2
					+ "]";
		}
		Soluzione s;
		double peso1;
		double peso2;
	}

}
