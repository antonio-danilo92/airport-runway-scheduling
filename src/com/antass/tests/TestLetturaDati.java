package com.antass.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Before;
import org.junit.Test;

import com.antass.core.Aereo;
import com.antass.core.LetturaDati;
import com.google.common.collect.Table;

public class TestLetturaDati {
	LetturaDati ld;
	@Before
	public void setUp() throws Exception {
		File f=new File("istanze/Arlanda-Run1.xlsx");
		System.out.println(f.getAbsolutePath());
		assertNotNull(f);
		assertTrue(f.exists());
		ld=new LetturaDati(f);
		assertNotNull(ld);
	}

	@Test
	public void testLetturaAerei() {
		ArrayList<Aereo> aerei;
		try {
			aerei = ld.leggiAerei();
			assertNotNull(aerei);
			assertTrue(aerei.size()==40);
			for(Aereo a:aerei){
				assertNotNull(a.getNome());
				assertNotNull(a.getEarliest());
				assertNotNull(a.getLatest());
				assertNotNull(a.getTarget());
			}
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Test
	public void testLetturaMatrice() {
		Table<String,String,Double> matSep =ld.leggiMatriceSeparazione();
		assertNotNull(matSep);
		assertTrue(matSep.get("SAS024", "SAS105")==80);
	}
}
