package com.antass.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.antass.util.Hour;
import com.google.common.collect.Table;
import com.rits.cloning.Cloner;

public class Soluzione {
	private File fileDati; //Utile conservarsi il file dei dati per verificare l'effettiva ammissibilit� e per le stampe di debug
	private Table<String, String, Double> matriceSeparazione;

	public Soluzione(File f){
		fileDati=f;
		matriceSeparazione=new LetturaDati(f).leggiMatriceSeparazione();
		listaAerei= new ArrayList<Aereo>();
	}
	
	/**Aggiunge un nuovo aereo alla soluzione
	 * 
	 * @param a aereo
	 * @param effH orario effettivo di arrivo/decollo
	 */
	public void addAereo(Aereo a, Hour effH){
		a.setEffettivaH(effH);
		listaAerei.add(a);
	}
	public void addAereoPos(Aereo a, Hour effH, int pos){
		a.setEffettivaH(effH);
		listaAerei.add(pos, a);
	}
	/**
	 * @return dimensione della soluzione
	 */
	public int getSize(){
		return listaAerei.size();
	}
	
	/**
	 * 
	 * @param pos
	 * @return aereo in posizione pos
	 */
	public Aereo getAereo(int pos){
		return listaAerei.get(pos);
	}
	public void addAereo(Aereo a) {
		listaAerei.add(a);	
	}
	public Aereo removeAereo(Aereo a){
		if(listaAerei.remove(a))
		return a;
		else
		return null;	
	}
	
	/** 
	 * @return il valore della funzione obiettivo della soluzione
	 */
	public int getValoreFunzioneObiettivo(){
		int funzioneOb=0;
		for(Aereo a: listaAerei){
			int differenza=a.getEffettivaH().getSeconds()-a.getTarget().getSeconds();
			funzioneOb= funzioneOb+ Math.abs(differenza);
		}
		return funzioneOb;
	}
	
	public String toString(){
		String result="Aereo\t\t orario";
		for(Aereo a: listaAerei){
			result= result+ "\n"+a.getNome()+"\t\t "+a.getEffettivaH();
		}
		return result;
	}
	/** 
	 * @return se la soluzione � inammissibile restituisce di quanti secondi si � andati oltre il latest come somma fra tutti gli aerei
	 */
	public int getSforamento(){
		int sommaSforamenti=0;
		for(int i=0; i<listaAerei.size(); i++){
			int finestra=listaAerei.get(i).getLatest().getSeconds()-(listaAerei.get(i).getEffettivaH()).getSeconds();
			if(finestra<0){
				sommaSforamenti-=finestra;
			}
		}
		return sommaSforamenti;
	}
	public boolean isAmmissibile(){
		return getSforamento()==0;
	}
	public void stampaSoluzione(String nomeFile) throws IOException{
		stampaSoluzione(nomeFile,"Soluzione");
	}
	
	public void stampaSoluzione(String nomeFile,String nomeFoglio) throws IOException{
		Workbook wb;
		File f=new File(nomeFile);
		Sheet sheet;
		if(!f.exists()){
			wb=new XSSFWorkbook();
		}
		else{
			InputStream inp = new FileInputStream(f);
		    try {
				wb = WorkbookFactory.create(inp);
			} catch (Exception e) {
				System.err.println("impossibile aprire il file. � possibile che sia aperto da un altro programma o che non sia in formato xlsx");
				return;
			} 
		}
		if(wb.getSheetIndex(nomeFoglio)>=0){
			wb.removeSheetAt(wb.getSheetIndex(nomeFoglio));
	
		}

		sheet= wb.createSheet(nomeFoglio);
	    Row row = sheet.createRow((short)0);
	    // Create a cell and put a value in it.
	    Cell cell = row.createCell(0);
	    cell.setCellValue("Nome Aereo");
	    cell = row.createCell(1);
	    cell.setCellValue("Earliest");
	    row.createCell(2).setCellValue("Target");
	    row.createCell(3).setCellValue("Latest");
	    row.createCell(4).setCellValue("Orario assegnato");
	    row.createCell(5).setCellValue("Sforamento");
	    row.createCell(7).setCellValue("Separazione residua precedente 1");
	    row.createCell(8).setCellValue("Separazione residua precedente 2");
	    row.createCell(9).setCellValue("Separazione residua precedente 3");
	    row.createCell(10).setCellValue("Separazione residua precedente 4");

	    int sommaSforamenti=0;
	    for(int i=0; i<listaAerei.size(); i++){
		    row = sheet.createRow((short)i+1);
		    Aereo cur=listaAerei.get(i);
		    int j=1;
			int finestra=cur.getLatest().getSeconds()-(cur.getEffettivaH()).getSeconds();
			int sforamento= (finestra<0) ? -finestra : 0;
			sommaSforamenti+=sforamento;
		    row.createCell(0).setCellValue(cur.getNome());
		    row.createCell(1).setCellValue(""+cur.getEarliest());
		    row.createCell(2).setCellValue(""+cur.getTarget());
		    row.createCell(3).setCellValue(""+cur.getLatest());
		    row.createCell(4).setCellValue(""+cur.getEffettivaH());
		    row.createCell(5).setCellValue(sforamento);
		    while(j<=4 && i-j>=0){
		    	Aereo pre=listaAerei.get(i-j);
		    	row.createCell(6+j).setCellValue(cur.getEffettivaH().getSeconds()-pre.getEffettivaH().getSeconds()-matriceSeparazione.get(pre.getNome(), cur.getNome()));
		    	j++;
		    }
		   
		}
	    row = sheet.createRow(sheet.getLastRowNum()+3);
	    row.createCell(0).setCellValue("valore della funzione obiettivo: ");
	    row.createCell(6).setCellValue(this.getValoreFunzioneObiettivo());
	    row = sheet.createRow(sheet.getLastRowNum()+1);
	    row.createCell(0).setCellValue("La soluzione ha sforato per un totali di secondi: ");
	    row.createCell(6).setCellValue(sommaSforamenti);
		FileOutputStream fileOut = new FileOutputStream(f);

	    wb.write(fileOut);
		fileOut.close();
		wb.close();
	}
	public Soluzione clone(){
		Cloner cloner=new Cloner();
		return cloner.deepClone(this);
	}
	private ArrayList<Aereo> listaAerei;

	public void ricalcola() {
		for(int i=0;i<this.listaAerei.size();i++){
			Aereo curr = this.getAereo(i);
			Hour calcH=calcolaPartenza(this,curr,i-1);
			curr.setEffettivaH(calcH);
		}
		
	}

	//TODO da testare e commentare
	private Hour calcolaPartenza(Soluzione s, Aereo succ, int posPrecedente) {
		Aereo successivo=succ;
		Hour effettivaHMax=successivo.getEarliest();
		for(int i=0;i<4;i++){
			if(posPrecedente-i<0) break;
			Aereo tmp=s.getAereo(posPrecedente-i);
			int sepTmp=(int) Math.ceil(matriceSeparazione.get(tmp.getNome(), successivo.getNome()));
			Hour effettivaHMin=new Hour(tmp.getEffettivaH().getSeconds()+sepTmp);
			if(effettivaHMin.compareTo(effettivaHMax)>0){
				effettivaHMax=effettivaHMin;
			}
		}
		return effettivaHMax;
	}
	
}
