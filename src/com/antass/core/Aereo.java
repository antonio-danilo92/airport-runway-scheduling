package com.antass.core;

import com.antass.util.Hour;

public class Aereo implements Comparable<Aereo>{

	public Aereo(String nome, Hour earliest, Hour target, Hour latest) {
		this.nome = nome;
		this.earliest = earliest;
		this.target = target;
		this.latest = latest;
		this.effettivaH= Hour.parse("00.00.00");		
	}

	public Aereo (String nome){
		this.nome=nome;
	}
	
	@Override
	public boolean equals(Object obj) {
		Aereo cmp;
		if(obj instanceof Aereo) cmp=(Aereo) obj;
		else return false;
		return nome.equals(cmp.nome);
	}

	public String getNome(){
		return nome;
	}
	
	@Override
	public String toString() {
		return "" + nome + "\t" + earliest + "\t"+ target + "\t" + latest + "\t" + effettivaH;
	}

	public Hour getEarliest() {
		return earliest;
	}

	public void setEarliest(Hour earliest) {
		this.earliest = earliest;
	}

	public Hour getTarget() {
		return target;
	}
	
	public Hour getEffettivaH(){
		return effettivaH;
	}

	public void setTarget(Hour target) {
		this.target = target;
	}

	public Hour getLatest() {
		return latest;
	}

	public void setLatest(Hour latest) {
		this.latest = latest;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setEffettivaH(Hour h){
		this.effettivaH=h;
	}
	
	public int compareTo(Aereo a) {
		return this.target.compareTo(a.getTarget());
	}

	private String nome;
	private Hour earliest;
	private Hour target;
	private Hour latest;
	private Hour effettivaH;		// effettivaH rappresenta il reale orario di partenza o di decollo dell'aereo
	
	
}
