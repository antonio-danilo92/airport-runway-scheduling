package com.antass.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import com.antass.core.Greedy.flags;
import com.antass.exception.InammissibilitaException;
import com.antass.util.Costanti;
import com.antass.util.Hour;
import com.google.common.collect.Table;

public class TwoOpt {
	private Table<String, String, Double> matriceSeparazione;
	private File f; //conservo il nome del file per debug. Se stampo i risultati, li stampo con il nome del foglio da cui ho preso i dati
	private int k=0; //variabile utile per il debug. Serve per stampare i fari passi della soluzione su fogli con nome diverso

	public TwoOpt(String nomeFileInput){
		f=new File(nomeFileInput);
		LetturaDati ld=new LetturaDati(f);
		this.matriceSeparazione=ld.leggiMatriceSeparazione();
	}
	/**
	 * Metodo che rende ammissibile una soluzione se possibile.
	 * Nel caso in cui non riesca a renderla ammissibile, lancia un'eccezione
	 *
	 * @param soluzione da rendere ammissibile
	 * @return soluzione ammissibile
	 * @throws InammissibilitaException 
	 */
	public Soluzione ammissibile(Soluzione s) throws InammissibilitaException{
		boolean mov=true;
		while (!s.isAmmissibile() && mov) {
			mov=false;
			for (int i = s.getSize()-1; i > 0; i--) {
				Aereo a = s.getAereo(i);
				int finestra = a.getLatest().getSeconds() - (a.getEffettivaH()).getSeconds();
				if (finestra < 0 && i > 2) {
					Soluzione tmp=swapAmmissibilita(s,i);	
					if(tmp!=null){
						s=tmp;
						mov=true;
					}
				}
			}
		}
		if(!s.isAmmissibile()) throw new InammissibilitaException("Impossibile rendere ammissibile la soluzione");
		return s;
	}
	/**
	 * Metodo che ottimizza la soluzione facendo scambi di aerei a due a due
	 * l'aereo candidato allo scambio � un aereo che ha un certo margine di ritardo
	 * si prova a scambiarlo con i 4 precedenti o i 4 successivi
	 * se si ottengono miglioramenti alla funzione obiettivo allora lo scambio viene realmente effettuato
	 *
	 * @param soluzione da ottimizzare
	 * @return soluzione ottimizzata
	 */
	public Soluzione ottimizza(Soluzione s){
		boolean mov=true; /*mov mi dice se nella corrente iterazione ho fatto almeno uno scambio. 
		se non ne ho fatto nessuno allora non si possono pi� fare ottimizzazioni
		*/
		EuristicaFinestre ecf;
		try {
			ecf=new EuristicaFinestre(f.getCanonicalPath());
			s=ecf.ottimizza(s);
		} catch (IOException e) {
			System.err.println("Il file non esiste o � aperto da un altro programma. L'ottimizzazione finestre non verr� eseguita");
		}
		while(mov){

			mov=false;
			for (int i = 2; i < s.getSize(); i++) { // per ogni aereo
				Aereo a = s.getAereo(i);
				int finestra = (a.getTarget()).getSeconds()-a.getEffettivaH().getSeconds(); //calcolo il ritardo/anticipo dell'aereo i
				/*Calcolo il massimo ritardo o anticipo di i, in base alla variabile finestra
				 * Se l'aereo � in anticipo, calcolo il massimo anticipo
				 * se l'aereo � in ritardo, calcolo il massimo ritardo
				 */
				int maxRitardo= (finestra>0) ? a.getTarget().getSeconds()-a.getEarliest().getSeconds() : a.getLatest().getSeconds()-a.getTarget().getSeconds();
	
				if (Math.abs(finestra)>=38) {//if che si basa su plain value. Bisogna capire quale dei due funziona meglio		
					//se l'aereo i fa abbastanza ritardo/anticipo, allora prova a scambiarlo con altri aerei
					Soluzione tmpsol=swapRicercaLocale(s,i); 
					if(tmpsol!=null) { //Se swapRicercaLocale ha restituito null, allora nessuno scambio ha portato benefici
						s=tmpsol;
						mov=true; 
					}
					
				}
			}
		}
		return s;
	}
	/**
	 * Metodo che prova a scambiare l'aereo i con i suoi 4 precedenti o 4 successivi
	 *
	 * @param Soluzione s, la soluzione di partenza
	 * @param int i, l'aereo da scambiare
	 * @return Soluzione con lo scambio effettuato, oppure null se nessuno scambio ha ridotto la funzione obiettivo
	 */
	private Soluzione swapRicercaLocale(Soluzione s, int i) {
		int numIterazioni=0; //numero di aerei che si � provato a scambiare finora
		Soluzione clone;
		Aereo a = s.getAereo(i);
		EuristicaFinestre ecf=null;
		try {
			ecf=new EuristicaFinestre(f.getCanonicalPath());
		} catch (IOException e) {
			System.err.println("Il file non esiste o � aperto da un altro programma. L'ottimizzazione finestre non verr� eseguita");
		}
		//int finestra = (a.getTarget()).getSeconds()-a.getEffettivaH().getSeconds();
		int comparaEffettivaTarget=a.getTarget().compareTo(a.getEffettivaH()); // ritardo o anticipo dell'aereo
		int j=(comparaEffettivaTarget>0) ? i+1 : i-1; //se � un ritardo, allora provo a scambiarlo col precedente, altrimenti provo lo scambio col successivo
		do{
			if(j<0 || j>s.getSize()) return null; //se sforo i limiti dell'array di aerei, faccio return
			clone=s.clone(); //clono la soluzione in modo che le modifiche non siano permanenti
			swapAerei(clone,i,j); //scambio l'aereo i e j
			if(ecf!=null)
				clone=ecf.ottimizza(clone);
			degugPrintOtt(s, i, clone, j);
			j=(comparaEffettivaTarget>0) ? j+1 : j-1; //il prossimo aereo da provare � il successivo se l'aereo i era in ritardo, altrimenti sar� il precedente
			numIterazioni++;
			
		}while((clone.getValoreFunzioneObiettivo()>=s.getValoreFunzioneObiettivo() || !clone.isAmmissibile()) && numIterazioni<4);
		
		if(clone.getValoreFunzioneObiettivo()<s.getValoreFunzioneObiettivo() && clone.isAmmissibile()) {
			System.err.println("Lo scambio " + s.getAereo(i).getNome() + " "
					+ s.getAereo(j+1).getNome()+" ha ridotto la funzione obiettivo, quindi � stato effettuato");
			return clone;
		}
		System.err.println("Lo scambio di " + s.getAereo(i).getNome() + "Non ha portato ad alcuna riduzione della funzione obiettivo");
		return null;
	}
	private void degugPrintOtt(Soluzione s, int i, Soluzione clone, int j) {
		if(Costanti.DEBUG){ //stampe di debug
			System.err.println("Provo lo scambio " + s.getAereo(i).getNome() + " "
				+ s.getAereo(j).getNome());
			System.err.println("Funzione obiettivo prima dello scambio: "+s.getValoreFunzioneObiettivo());
			System.err.println("Funzione obiettivo dopo lo scambio: "+clone.getValoreFunzioneObiettivo());
			System.err.println("La soluzione � ammissibile?: "+clone.isAmmissibile());
			System.err.println("\n");

			try {
				clone.stampaSoluzione("results/TestTwoOpt"+f.getName()+".xlsx", "iterazione "+k++);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private Soluzione swapAmmissibilita(Soluzione s, int i) {
		int j=i-1;
		Soluzione clone;
		Soluzione best=s;
		int numIterazioni=0;
		boolean mov=false;
		do{
			if(j<0) return null; //se sforo i limiti dell'array di aerei, faccio return
			clone=s.clone();
			swapAerei(clone,i,j);
			if(Costanti.DEBUG){
				System.err.println("Provo lo scambio " + clone.getAereo(i).getNome() + " "
					+ clone.getAereo(j).getNome());
				System.err.println("Sforamento prima dello scambio: "+s.getSforamento());
				System.err.println("Sforamento Dopo lo scambio: "+clone.getSforamento());
				try {
					clone.stampaSoluzione("results/TestTwoOpt"+f.getName()+".xlsx", "iterazione "+k++);
				} catch (IOException e) {
					System.out.println("Impossibile scrivere il file. Potrebbe essere aperto da un altro programma");
				}
			}
			if(clone.getSforamento()<best.getSforamento()){
				best=clone;
				mov=true;
			}
			j--;
			numIterazioni++;
		}while(/*clone.getSforamento()>=s.getSforamento() && */numIterazioni<4);
		if(mov){
		System.err.println("Lo scambio " + clone.getAereo(i).getNome() + " "
				+ clone.getAereo(j+1).getNome()+" ha portato benefici, quindi � stato effettuato");
		return best;
		}
		return null;
	}

	private void swapOttimizzazione(Soluzione s, int i){
		this.swapAerei(s, i, i-1);
	}
	private void swapAerei(Soluzione s, int i,int j){
		if(i<j){ //per semplificare l'algoritmo, questo funziona solo se i>j. Pertanto se accade che i<j, li scambio. Soluzione schifosa ma se poi teniamo tempo la aggiustiamo
			int tmp;
			tmp=j;
			j=i;
			i=tmp;
		}
		Aereo daScambiare2= s.removeAereo(s.getAereo(i));
		Aereo daScambiare1= s.removeAereo(s.getAereo(j));
		s.addAereoPos(daScambiare2, null, j);
		s.addAereoPos(daScambiare1, null, i);
		s.ricalcola();
	}


}
