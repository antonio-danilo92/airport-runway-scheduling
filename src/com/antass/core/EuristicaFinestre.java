package com.antass.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import com.antass.util.Hour;
import com.google.common.collect.Table;

public class EuristicaFinestre {
	
	public EuristicaFinestre(String nomeFileInput){
		File f=new File(nomeFileInput);
		LetturaDati ld=new LetturaDati(f);
		this.matriceSeparazione=ld.leggiMatriceSeparazione();
	}
/**
 * metodo per ottimizzare le finestre nella soluzione s	
 * @param s soluzione da ottimizzare
 * @return soluzione ottimizzata
 */
	
	public Soluzione ottimizza(Soluzione s){
		//file di Debug
		File outF= new File("results/output.txt");
		try {
			p= new PrintStream (outF);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/** per ogni elemento della soluzione, a partire dal secondo verifico se c'� una finestra
		 */
		for(int i=1; i<s.getSize(); i++){
			boolean presenzaFinestra= isFinestra(s, i);
			/**se la finestra � presente la calcolo**/
			if(presenzaFinestra){
				int finestra= calcolaFinestra(s, i);
				if(finestra>0){
					/**verifico se l'orario di partenza dell'aereo precedente � minore del target e quindi pu� essere scalato**/
					Aereo a= s.getAereo(i-1);
					if(a.getEffettivaH().compareTo(a.getTarget())<0){
						p.println("finestra di "+finestra+ " tra "+s.getAereo(i).getNome()+" e"+ a.getNome());
						scala(s,i-1,finestra);
					}
					else
						/**se l'orario di partenza � superiore al target, non ha senso scalare per allontanarsi dal target**/
						p.println("c'� finestra e mi dovrei muovere a sinistra ma non posso farlo"+a.getNome()+" "+s.getAereo(i).getNome());
						
				}
			}
		}
		return s;
	}

	private Soluzione scala(Soluzione s, int j, int spostamento){
		/** il gap iniziale � settato uguale alla finestra rilevata**/
		int gap= spostamento;
		/**se esco fuori dagli indici dell'arrey ritorno la soluzione attuale**/
		if(j<0)
			return s;
		else{
			/**prendo l'aereo che vorrei scalare e calcolo l'ipotetica ora a cui potrebbe partire scalandolo dell'intera finestra presente**/
			Aereo a= s.getAereo(j);
			Hour hIpotetica= new Hour(a.getEffettivaH().getSeconds()+gap);
			/**calcolo l'orario effettivo a cui lo posso assegnare per non violare i vincoli della matrice**/
			Hour hAssegnare= trovaMin(s, j, hIpotetica);
			/**se l'orario da assegnare � maggiore del target allora mi fermo al target**/
			if(hAssegnare.compareTo(a.getTarget())>0){
				p.println(hAssegnare+" > target, mi metto sul target di: "+ a.getNome());
				/**calcolo il nuovo gap per scalare gli altri aerei**/
				gap= gap- (hAssegnare.getSeconds()-a.getTarget().getSeconds());
				hAssegnare= a.getTarget();
				p.println("\tgap= "+gap);
				/**se il gap diventa negativo ho riempito pi� che potevo la finestra e non posso pi� scalare niente**/
				if(gap<=0){
					p.println("non posso pi� scalare gap= "+gap);
					return s;
				}
			}
			else{
				/**se l'orario � inferiore al target mi setto proprio li**/
				p.println(hAssegnare+" < target per "+ a.getNome());
				p.println("\tgap= "+gap);
			}
			a.setEffettivaH(hAssegnare);
			j--;
			/**rinvoco la funzione considerando l'elemento precedente da scalare**/
			return scala(s,j,gap);
		}
	}
	/**
	 * funzione che mi permette di trovare l'orario minimo di partenza senza violare i vincoli della matrice
	 * @param s
	 * @param j
	 * @param hIpotetica orario migliore a cui lo posso settare data la finestra rilevata
	 * @return
	 */
	private Hour trovaMin(Soluzione s,int j, Hour hIpotetica) {
		Aereo a= s.getAereo(j);
		Hour effettivaHMin= hIpotetica;
		/**analizzo i 4 aerei successivi a quello che voglio scalare**/
		for(int i=1; i<5; i++){
			if(j+i>= s.getSize()) break;
			Aereo tmp=s.getAereo(j+i);
			p.println("controllo matrice tra "+a.getNome()+" "+tmp.getNome());
			int sepTmp=(int) Math.ceil(matriceSeparazione.get(a.getNome(),tmp.getNome()));
			Hour effettivaHMax=new Hour(tmp.getEffettivaH().getSeconds()-sepTmp);
			/**se l'orario previsto dai valori della matrice � minore del mio miglior orario setto il migliore al nuovo orario trovato**/
			if(effettivaHMax.compareTo(effettivaHMin)<0){
				effettivaHMin=effettivaHMax;
				p.println("ho sforato la matrice con "+ tmp.getNome()+" e quindi posso spostarmi al massimo a: "+ effettivaHMin);
			}
		}
		return effettivaHMin;
	}


	private int calcolaFinestra(Soluzione s, int i) {
		Aereo a1= s.getAereo(i-1);
		Aereo a2= s.getAereo(i);
		int valoreSeparazione= (int) Math.ceil(matriceSeparazione.get(a1.getNome(), a2.getNome()));
		int earliest= a2.getEarliest().getSeconds();
		int hEff= a1.getEffettivaH().getSeconds();
		int finestra =earliest-hEff-valoreSeparazione;
		return finestra;
	}


	private boolean isFinestra(Soluzione s, int i) {
		Aereo a2= s.getAereo(i);
		if((a2.getEffettivaH().compareTo(a2.getEarliest())==0))
			return true;
		else 
			return false;
	}

	
	private Table<String, String, Double> matriceSeparazione;
	private PrintStream p;

}
