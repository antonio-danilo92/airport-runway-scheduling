package com.antass.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.antass.exception.InammissibilitaException;

public class EuristicaAirportScheduling {
	public static ArrayList<Soluzione> calcola(String path) throws InammissibilitaException, InvalidFormatException, NullPointerException, IOException{
		return calcola(path,true);
	}
	public static ArrayList<Soluzione> calcola(String path,boolean xlsPrint) throws InammissibilitaException, InvalidFormatException, NullPointerException, IOException{
		double peso1=0.9;
		double peso2=0.1;
		Greedy g;
		//try {
			g = new Greedy(path, peso1, peso2);
		/**} catch (InvalidFormatException e1) {
			System.err.println("Il file selezionato � in un formato non corretto. Si prega di selezionar un file con estensione xlsx");
			return null;
		} catch (IOException e1) {
			System.err.println("Impossibile aprire il file selezionato. Potrebbe essere aperto da un altro programma");
			return null;
		}*/
		Soluzione greedy= g.eseguiAlgoritmo();
		TwoOpt to=new TwoOpt(path);
		Soluzione ammissibile=to.ammissibile(greedy);
		Soluzione ottimizzata=to.ottimizza(ammissibile);
		if(xlsPrint)
			print(greedy, ammissibile, ottimizzata);
		ArrayList<Soluzione> ritorno= new ArrayList<Soluzione>();
		ritorno.add(greedy);
		ritorno.add(ammissibile);
		ritorno.add(ottimizzata);
		return ritorno;
	}

	private static void print(Soluzione greedy, Soluzione ammissibile, Soluzione ottimizzata) {
		String noneFileScittura="results/Risultato.xlsx";

		try {
			File f=new File(noneFileScittura);
			if(f.exists()) f.delete();
			greedy.stampaSoluzione(noneFileScittura, "Risultato Greedy ES");
			ammissibile.stampaSoluzione(noneFileScittura, "Risultato Ammissibilit� ES");
			ottimizzata.stampaSoluzione(noneFileScittura, "Risultato Ottimizzazione ES");

		} catch (IOException e) {
			System.err.println("Impossibile aprire il file "+noneFileScittura+". � possibile che il file sia aperto da un altro programma");
		}
	}
}
