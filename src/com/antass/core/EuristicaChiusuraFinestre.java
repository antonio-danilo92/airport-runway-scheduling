package com.antass.core;

import java.io.File;

import com.antass.util.Hour;
import com.google.common.collect.Table;

public class EuristicaChiusuraFinestre {
	
	public EuristicaChiusuraFinestre(String nomeFileInput){
		File f=new File(nomeFileInput);
		LetturaDati ld=new LetturaDati(f);
		this.matriceSeparazione=ld.leggiMatriceSeparazione();
	}
	
	
	public Soluzione ottimizza(Soluzione s){
		//gestione primo elemento
		Hour hMinima=new Hour(s.getAereo(1).getEffettivaH().getSeconds()- (int) Math.ceil(matriceSeparazione.get(s.getAereo(0).getNome(), s.getAereo(1).getNome())));
		if(hMinima.compareTo(s.getAereo(0).getTarget())<0)
			s.getAereo(0).setEffettivaH(hMinima);
		else
			s.getAereo(0).setEffettivaH(s.getAereo(0).getTarget());
		
		
		for(int i=1; i<s.getSize(); i++){
			boolean presenzaFinestra= isFinestra(s, i);
			if(presenzaFinestra){
				int finestra= calcolaFinestra(s, i);
				if(finestra>0){
					Aereo a= s.getAereo(i-1);
					if(a.getEffettivaH().compareTo(a.getTarget())<0){
						int spostamento;
						Hour hConFinestra= new Hour(a.getEffettivaH().getSeconds()+finestra);
						if(hConFinestra.compareTo(a.getTarget())<0){
							spostamento= hConFinestra.getSeconds()- a.getEffettivaH().getSeconds();
							System.out.println("vorrei settarmi prima del target: "+ a.getNome()+" Spostamento:" + spostamento);
							if(controllaNonTriangolare(s, i-1, hConFinestra)){
								a.setEffettivaH(hConFinestra);
								System.out.println("mi setto prima del target: "+ a.getNome()+" Spostamento:" + spostamento);
								//se ho fatto uno spostamento potrei scalare anche gli aerei precedenti e avvicinarli al target solo se l'orario effettivo di partenza � minore del target 
								scala(s,i-2, spostamento);
							}
						}
						else{
							spostamento=a.getTarget().getSeconds()- a.getEffettivaH().getSeconds();
							if(controllaNonTriangolare(s, i-1, a.getTarget())){
								System.out.println("vorrei settarmi proprio sul target "+a.getNome()+" Spostamento:" + spostamento);
								a.setEffettivaH(a.getTarget());
								System.out.println("mi setto proprio sul target "+a.getNome()+" Spostamento:" + spostamento);
								//se ho fatto uno spostamento potrei scalare anche gli aerei precedenti e avvicinarli al target solo se l'orario effettivo di partenza � minore del target 
								scala(s,i-2, spostamento);
							}
						}
					}
					else
						System.err.println("c'� finestra e mi dovrei muovere a sinistra ma non posso farlo"+a.getNome()+" "+s.getAereo(i).getNome());
						
				}
			}
		}
		return s;
	}
	
	private Soluzione scala(Soluzione s, int j, int spostamento){
		if(j<0)
			return s;
		else{
			Aereo a= s.getAereo(j);
			if(a.getEffettivaH().compareTo(a.getTarget())<0){
				System.out.print(a.getNome()+ "partiva alle: "+ a.getEffettivaH());
				Hour nuovoOrario=new Hour(a.getEffettivaH().getSeconds()+spostamento);
				if(this.controllaNonTriangolare(s, j, nuovoOrario)){
					if(nuovoOrario.compareTo(a.getTarget())<0){
						a.setEffettivaH(nuovoOrario);
						System.out.println(" scalato in avanti di "+ spostamento);
					}
					else{
						a.setEffettivaH(a.getTarget());
						System.out.println(" scalato in avanti di "+ spostamento +"fermato al target");
						spostamento= spostamento - (nuovoOrario.getSeconds()-a.getTarget().getSeconds());
					}
					j--;
					return scala(s, j, spostamento);
				}
				else{
					System.out.println("Problema potrei scalare ma disuguaglianza non rispettata");
					return s;
				}
			}
			else return s;
		}
	}
	
	private int calcolaFinestra(Soluzione s, int i) {
		Aereo a1= s.getAereo(i-1);
		Aereo a2= s.getAereo(i);
		int valoreSeparazione= (int) Math.ceil(matriceSeparazione.get(a1.getNome(), a2.getNome()));
		int earliest= a2.getEarliest().getSeconds();
		int hEff= a1.getEffettivaH().getSeconds();
		int finestra =earliest-hEff-valoreSeparazione;
		return finestra;
	}


	private boolean isFinestra(Soluzione s, int i) {
		Aereo a2= s.getAereo(i);
		if((a2.getEffettivaH().compareTo(a2.getEarliest())==0))
			return true;
		else 
			return false;
	}

	private boolean controllaNonTriangolare(Soluzione s, int i, Hour hIpotetica){
		System.out.println("controllo disuguaglianza");
		int j=1;
		boolean corretto= true;
		Aereo rif= s.getAereo(i);
		while(j<4 && j< s.getSize()){
			Aereo a= s.getAereo(i+j);
			int dist= (int) Math.ceil(matriceSeparazione.get(rif.getNome(), a.getNome()));
			Hour hRichiesta= new Hour (a.getEffettivaH().getSeconds()-dist);
			j++;
			System.out.println("****controllo: "+ rif.getNome()+" "+a.getNome()+" "+hRichiesta+" "+hIpotetica);
			if(hRichiesta.compareTo(hIpotetica)<0){
				corretto= false;
				System.out.println("problema disuguaglianza aerei: "+ rif.getNome()+" "+a.getNome()+" HRichiesta "+hRichiesta+" < HIpotetica"+hIpotetica);
				j=4;
			}
			
		}
		return corretto;
	}

	private Table<String, String, Double> matriceSeparazione;


}
