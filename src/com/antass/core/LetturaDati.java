package com.antass.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.antass.util.Hour;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

public class LetturaDati {
	private File file;
	/**
	 * Costruttore
	 *
	 * @param file il file cui leggere i dati 
	 */
	public LetturaDati(File file){
		this.file=file;
        
        
	}
	
	
	private ArrayList<Aereo> leggiFogliAerei(XSSFWorkbook workbook) {
		ArrayList<Aereo> aerei = new ArrayList<Aereo>();
		for (int i = 1; i <= 2; i++) {
			XSSFSheet sheet = workbook.getSheetAt(i);
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next(); //le prime due righe non contengono dati, quindi vengono skippate
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				String airplaneName = row.getCell(0).getStringCellValue();
				Date dateEarliest = row.getCell(4).getDateCellValue();
				Date dateTarget = row.getCell(5).getDateCellValue();
				Date dateLatest = row.getCell(6).getDateCellValue();
				Hour earliest = Hour.parse(dateEarliest);
				Hour latest = Hour.parse(dateLatest);
				Hour target = Hour.parse(dateTarget);
				aerei.add(new Aereo(airplaneName, earliest, target, latest));
			}
		}
		return aerei;
	}

	/**
	 * Metodo che legge gli aerei da file 
	 *
	 * @return restituisce la lista sotto forma di instanze della classe aereo
	 * @throws IOException 
	 * @throws InvalidFormatException 
	 */
	public ArrayList<Aereo> leggiAerei() throws InvalidFormatException, IOException{
		FileInputStream fis;
		XSSFWorkbook workbook;
	//	try {
			fis = new FileInputStream(file);
			workbook = new XSSFWorkbook(file);
/**
		} catch (FileNotFoundException e1) {
			System.err.println("impossibile trovare il file: "+ file.getAbsolutePath());
			return null;
		} catch (InvalidFormatException e) {
			System.err.println("Il file "+ file.getAbsolutePath()+ " non � un file xlsx");
			return null;
		} catch (IOException e) {
			System.err.println("IO exception");
			return null;
		}*/
        ArrayList<Aereo> aerei= leggiFogliAerei(workbook);		
        try {
			fis.close();
		} catch (IOException e) {
			System.err.println("Impossibile chiudere il file");
		}
        return aerei;
	}
	
	/**
	 * Metodo che legge la matrice di separazione da file
	 *
	 * @return restituisce la matrice di separazione utilizzando una Table 
	 * dalla libreria Google Guava. La table funziona in modo simile ad una Map:
	 * La Map possiede una chiave e ad ogni chiave corrisponde un valore.
	 * Pertanto il metodo Map.get(String s) restituir� l'oggetto corrispondende alla chiave s
	 * 
	 * La table possiede invece 2 chiavi ed un solo risultato. Table.get(String s1, String s2) 
	 * restituisce il valore corrispondente alle due chiavi. Per maggiori informazioni sul 
	 * funzionamento della Table:
	 * @see <a href="http://guava-libraries.googlecode.com/svn/tags/release09/javadoc/com/google/common/collect/Table.html">Guava</a>
	 * 
	 * 
	 */
	public Table<String,String,Double> leggiMatriceSeparazione(){
		FileInputStream fis;
		XSSFWorkbook workbook;

		try {
			fis = new FileInputStream(file);
			workbook = new XSSFWorkbook(file);

		} catch (FileNotFoundException e1) {
			System.err.println("impossibile trovare il file: "
					+ file.getAbsolutePath());
			return null;
		} catch (InvalidFormatException e) {
			System.err.println("Il file " + file.getAbsolutePath()
					+ " non � un file xlsx");
			return null;
		} catch (IOException e) {
			System.err.println("IO exception");
			return null;
		}
		Table<String, String, Double> matSep = leggiFoglioSep(workbook);
		try {
			fis.close();
		} catch (IOException e) {
			System.err.println("Impossibile chiudere il file");
		}
		return matSep;
		
	}


	private Table<String, String, Double> leggiFoglioSep(XSSFWorkbook workbook) {
		XSSFSheet sheet = workbook.getSheetAt(3);
		int nRow=sheet.getPhysicalNumberOfRows();
		String tmp;
		Row headerRow=sheet.getRow(0);
		ArrayList<String> headerNames=new ArrayList<String>();
		int nCol=headerRow.getPhysicalNumberOfCells();
		Table<String, String, Double> matSep= HashBasedTable.create(nRow, nCol);
		for(int i=0;i<nCol;i++){
			tmp=headerRow.getCell(i).getStringCellValue();
			headerNames.add(tmp);
		}
		for(int i=1;i<nRow;i++){
			Row curRow=sheet.getRow(i);
			for(int j=1;j<nCol;j++){
				if(j!=i){
					double value =curRow.getCell(j).getNumericCellValue();
					String key1=headerNames.get(i);
					String key2=headerNames.get(j);
					matSep.put(key1, key2, value);
				}
			}
		}
		return matSep;
	}
}
