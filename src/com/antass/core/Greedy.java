package com.antass.core;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.antass.util.Hour;
import com.google.common.collect.Table;

public class Greedy {
	
	public Greedy(String nomeFileInput, double peso1, double peso2, flags... flags) throws InvalidFormatException, IOException, NullPointerException{
		this.peso1=peso1;
		this.peso2=peso2;
		dataFile=new File(nomeFileInput);
		LetturaDati ld=new LetturaDati(dataFile);
		this.aerei= ld.leggiAerei();
		this.matriceSeparazione=ld.leggiMatriceSeparazione();
		fl=Arrays.asList(flags);
	}
	
	/**
	 * ordino gli aerei in ordine crescente di target
	 */
	private void ordinaTargetCrescente(){
		Collections.sort(aerei);
	}
	
	public Soluzione eseguiAlgoritmo(){
		ordinaTargetCrescente();
		Soluzione soluzione= new Soluzione(dataFile);
		Hour tempo=new Hour(0);					
		Aereo greedy=aerei.get(0);				//il primo aereo greedy � quello con il target pi� basso
		tempo=calcolaPartenza(soluzione,greedy);
		soluzione.addAereo(greedy, tempo);
		aerei.remove(greedy);					//elimino il primo aereo dalla lista di aerei da ordinare
		while(!aerei.isEmpty()){					//fino a quando non ho sistemato tutti gli aerei
			greedy= calcolaGreedy(tempo, soluzione);	//cerco il migliore
			Hour tempoPartenza=calcolaPartenza(soluzione,greedy);
			soluzione.addAereo(greedy, tempoPartenza);		//lo aggiungo alla soluzione
			aerei.remove(greedy);					//e lo rimovo dalla lista degli aerei non ancora ordinati
			tempo=tempoPartenza;				//setto il tempo attuale 	
		}
		return soluzione;
	}
	
	
	
	//TODO da testare e commentare
	private Hour calcolaPartenza(Soluzione s, Aereo succ, int posPrecedente) {
		Aereo successivo=succ;
		Hour effettivaHMax=successivo.getEarliest();
		for(int i=0;i<4;i++){
			if(posPrecedente-i<0) break;
			Aereo tmp=s.getAereo(posPrecedente-i);
			int sepTmp=(int) Math.ceil(matriceSeparazione.get(tmp.getNome(), successivo.getNome()));
			Hour effettivaHMin=new Hour(tmp.getEffettivaH().getSeconds()+sepTmp);
			if(effettivaHMin.compareTo(effettivaHMax)>0){
				effettivaHMax=effettivaHMin;
			}
		}
		return effettivaHMax;
	}
	private Hour calcolaPartenza(Soluzione s, Aereo succ) {
		return calcolaPartenza(s,succ,s.getSize()-1);
	}
	private Aereo calcolaGreedy(Hour h, Soluzione s) {
		Aereo precedente=s.getAereo(s.getSize()-1); //l'ultimo aereo sar� il precedente di quello che inserir�
		Aereo greedy= null;
		double minimo= Integer.MAX_VALUE;
		for(Aereo a: aerei){
			double valSep= Math.ceil(matriceSeparazione.get(precedente.getNome(), a.getNome()));
			double vicinanza=a.getTarget().getSeconds()- h.getSeconds()+valSep; //TODO viene preferito un aereo con valSep alto
			double finestra= a.getLatest().getSeconds()-h.getSeconds()+valSep;		
			double punteggio=vicinanza*peso1+finestra*peso2;
			//System.out.println("Nome aereo: "+a.getNome()+"Target time: "+a.getTarget().getSeconds()+" punteggio:" + punteggio+" ValSep: "+valSep);
			if(punteggio<minimo){
				greedy=a;
				minimo=punteggio;
			}
		}
		
		return greedy;
	}


	
	
	private boolean isSchedulabile(Aereo prop,Soluzione s) {
		int lastPos=s.getSize()-1;
		Aereo tmpAereo;
		int matVal;
		Hour propEffettivaH=calcolaPartenza(s,prop);
		int i=1;
		while(i<=4 && lastPos-i>0){
			tmpAereo= s.getAereo(lastPos-i);
			matVal=(int) Math.ceil(matriceSeparazione.get(tmpAereo.getNome(), prop.getNome()));
			int difference=propEffettivaH.getSeconds()-tmpAereo.getEffettivaH().getSeconds()-matVal;
			if(difference<0) return false;
			i++;
		}
		return true;
	}


	private double peso1;
	private double peso2;
	private ArrayList<Aereo> aerei;
	private Table<String, String, Double> matriceSeparazione;
	private List<flags> fl;
	private File dataFile;
	public enum flags{ //flags legacy. mantengono la compatibilit�, ma non pi� necessari
		FLAG_SWAP,
		FLAG_EARLIEST
	}
	
}
