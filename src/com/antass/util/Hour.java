package com.antass.util;

import java.util.Date;


public class Hour implements Comparable<Hour>{
	/**La classe rappresenta l'ora della giornata 
	 * Utilizza la variabile seconds per tenere traccia dell'ora 
	 * seconds indica i secondi passati dalla mezzanotte
	 * questa rappresentazione facilita e velocizza le comparazioni fra le ore
	 */
	private int seconds;
	
	public Hour(int sec){
		seconds=sec;
	}
	/**
	 * Metodo factory
	 * 
	 *
	 * @param Riceve una stringa nel formato hh.mm.ss 
	 * @return restituisce un oggetto Hour 
	 */
	public static Hour parse(String hString){
		
		String[] hms=hString.split("\\.");
		int sec=Integer.parseInt(hms[0])*3600+Integer.parseInt(hms[1])*60+Integer.parseInt(hms[2]);
		return new Hour(sec);
		
	}
	@Override
	public String toString() {
		 int hours = seconds / 3600;
		 int minutes = (seconds % 3600) / 60;
		 int seconds = this.seconds % 60;
		 return twoDigitString(hours) + "." + twoDigitString(minutes) + "." + twoDigitString(seconds);

	}
	private String twoDigitString(int number) {

	    if (number == 0) {
	        return "00";
	    }

	    if (number / 10 == 0) {
	        return "0" + number;
	    }

	    return String.valueOf(number);
	}
	
	public int compareTo(Hour toCompare) {
		if(seconds>toCompare.getSeconds()) return 1;
		else if(seconds<toCompare.getSeconds()) return -1;
		return 0;
	}
	public int getSeconds() {
		return seconds;
	}
	
	@SuppressWarnings("deprecation")
	public static Hour parse(Date d) {
		int sec=d.getHours()*3600+d.getMinutes()*60+d.getSeconds();
		return new Hour(sec);
	}

	
	
}
